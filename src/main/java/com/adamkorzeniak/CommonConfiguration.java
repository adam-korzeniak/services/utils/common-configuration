package com.adamkorzeniak;

import com.adamkorzeniak.utils.configuration.YamlPropertySourceFactory;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan
@EntityScan
@EnableEncryptableProperties
@PropertySource(value = "classpath:common-properties.yml", factory = YamlPropertySourceFactory.class)
public class CommonConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
