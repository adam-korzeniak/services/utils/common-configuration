package com.adamkorzeniak.utils.configuration;

import com.adamkorzeniak.utils.configuration.test.utils.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class YamlPropertySourceFactoryTest {

    private final YamlPropertySourceFactory factory = new YamlPropertySourceFactory();

    @Test
    void CreatePropertySource_ResourceExists_PropertySourceCreated() {
        //given
        Resource resource = new ClassPathResource(TestData.TEST_RESOURCE_PATH);
        EncodedResource encodedResource = new EncodedResource(resource);

        //when
        PropertySource<?> propertySource = factory.createPropertySource(null, encodedResource);

        //then
        assertTrue(propertySource.containsProperty(TestData.TEST_PROPERTY_KEY));
        assertTrue(propertySource.containsProperty(TestData.COMPLEX_TEST_PROPERTY_KEY));
        assertTrue(propertySource.containsProperty(TestData.COMPLEX_OTHER_PROPERTY_KEY));

        assertThat(propertySource.getProperty(TestData.TEST_PROPERTY_KEY))
                .isEqualTo(TestData.TEST_PROPERTY_VALUE);
        assertThat(propertySource.getProperty(TestData.COMPLEX_TEST_PROPERTY_KEY))
                .isEqualTo(TestData.COMPLEX_TEST_PROPERTY_VALUE);
        assertThat(propertySource.getProperty(TestData.COMPLEX_OTHER_PROPERTY_KEY))
                .isEqualTo(TestData.COMPLEX_OTHER_PROPERTY_VALUE);

        assertFalse(propertySource.containsProperty(TestData.COMPLEX_NOT_EXISTING_PROPERTY_KEY));

        assertThat(propertySource.getName())
                .isEqualTo(TestData.TEST_RESOURCE_PATH);
    }

    @Test
    void CreatePropertySource_ResourceNotExists_ExceptionThrown() {
        //given
        Resource missingResource = new ClassPathResource(TestData.MISSING_RESOURCE_PATH);
        EncodedResource encodedMissingResource = new EncodedResource(missingResource);

        //expect
        assertThrows(IllegalStateException.class, () -> factory.createPropertySource(null, encodedMissingResource));
    }

}
