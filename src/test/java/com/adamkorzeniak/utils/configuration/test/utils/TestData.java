package com.adamkorzeniak.utils.configuration.test.utils;

public class TestData {
    public static final String TEST_RESOURCE_PATH = "test.yml";
    public static final String MISSING_RESOURCE_PATH = "nothing-here.yml";

    public static final String TEST_PROPERTY_KEY = "test";
    public static final String TEST_PROPERTY_VALUE = "testValue";
    public static final String COMPLEX_TEST_PROPERTY_KEY = "app.complex.test.property";
    public static final String COMPLEX_TEST_PROPERTY_VALUE = "example";
    public static final String COMPLEX_OTHER_PROPERTY_KEY = "app.complex.other";
    public static final String COMPLEX_OTHER_PROPERTY_VALUE = "otherValue";
    public static final String COMPLEX_NOT_EXISTING_PROPERTY_KEY = "app.complex";

}
